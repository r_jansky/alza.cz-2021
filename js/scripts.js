// VARIABLES
// text
var $monthly = 'mesíční';
var $yearly = 'roční';

// total savings
var $amountMonthly = '1 896';
var $amountYearly = '22 750';


// SCRIPTS
// text append
$("#year-month").append( $monthly );
$("#amount-total").append( $amountMonthly );

// switch - monthly
$("#switch-monthly").click(function () {
  $("#switch-monthly").addClass( "is-active" );
  $("#switch-yearly").removeClass( "is-active" );

  $("#year-month").text( $monthly );
  $("#amount-total").text( $amountMonthly );
  return false;
});

// switch - yearly
$("#switch-yearly").click(function () {
  $("#switch-yearly").addClass( "is-active" );
  $("#switch-monthly").removeClass( "is-active" );

  $("#year-month").text( $yearly );
  $("#amount-total").text( $amountYearly );
  return false;
});
